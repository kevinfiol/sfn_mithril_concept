const Actions = (update, deps) => ({
    setIsLoading: isLoading => update(state => {
        state.isLoading = isLoading;
        return state;
    }),

    clearData: () => update(state => {
        state.username = '';
        state.steamid = null;
        state.friends = [];
        state.stagedFriends = {};
        state.commonApps = [];
        return state;
    }),

    setView: view => update(state => {
        state.view = view;
        return state;
    }),

    setUsername: username => update(state => {
        state.username = username;
        return state;
    }),

    setFriends: friends => update(state => {
        state.friends = friends;
        return state;
    }),

    setSteamid: steamid => update(state => {
        console.log(steamid);
        state.steamid = steamid;
        return state;
    }),

    setCommonApps: commonApps => update(state => {
        state.commonApps = commonApps;
        return state;
    }),

    stageFriend: friend => update(state => {
        state.stagedFriends[friend.steamid] = friend;
        return state;
    }),

    unstageFriend: steamid => update(state => {
        delete state.stagedFriends[steamid];
        return state;
    })
});

export default Actions;