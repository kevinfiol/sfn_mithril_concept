import m from 'mithril';
import stream from 'mithril/stream';
import Actions from './Actions';
import AsyncActions from './AsyncActions';

import Steam from '../services/Steam';

const initialState = {
    isLoading: false,
    // view: 'friends',
    view: 'index',
    username: '',
    steamid: null,
    friends: [],
    stagedFriends: {},
    commonApps: []
};

const update = stream();
const state  = stream.scan((x, f) => f(x), initialState, update);
state.map(console.log);

const deps         = { Steam };
const actions      = Actions(update, deps);
const asyncActions = AsyncActions(actions, m.redraw, deps);
const allActions   = Object.assign({}, actions, asyncActions);

export { state, allActions as actions };