const AsyncActions = (actions, redraw, deps) => ({
    getSteamID: identifier => {
        actions.setIsLoading(true);

        return deps.Steam.getSteamID(identifier)
            .then(data => actions.setSteamid(data.steamid))
        ;
    },

    getFriends: steamid => {
        actions.setIsLoading(true);

        return deps.Steam.getFriends(steamid)
            .then(actions.setFriends)
        ;
    },

    getCommonApps: steamids => {
        actions.setIsLoading(true);

        return deps.Steam.getCommonApps(steamids)
            .then(actions.setCommonApps)
        ;
    },
});

export default AsyncActions;