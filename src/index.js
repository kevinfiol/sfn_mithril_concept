import 'ace-css/css/ace.min.css';
import './styles/main.css';

import m from 'mithril';
import { state, actions } from './state';

import UserSubmission from './views/UserSubmission';
import FriendList from './views/FriendList';
import GameList from './views/GameList';

import friends from '../tests/data/friends.json';
import commonApps from '../tests/data/commonApps.json';

// actions.setUsername('kebsteam');
// actions.setSteamid('76561197978726907');
// actions.setFriends(friends);
// actions.setCommonApps(commonApps);

const App = {
    view: ({ attrs: { state, actions } }) =>
        m('div',
            m(UserSubmission, { state, actions }),

            state.isLoading &&
                m('p', 'loading...')
            ,

            (state.view === 'index' && !state.isLoading) &&
                m('p', 'index page')
            ,

            (state.view === 'friends' && !state.isLoading) &&
                m(FriendList, { state, actions })
            ,

            (state.view === 'games' && !state.isLoading) &&
                m(GameList, { state, actions })
            ,
        )
};

m.mount(document.getElementById('app'), {
    view: () => m(App, { state: state(), actions })
});