import m from 'mithril';

// Mithril.Request
const { request } = m;

// API Url
// const API = endpoint => `https://steam-serv.herokuapp.com/app/${endpoint}/`;
const API = endpoint => `http://localhost:80/app/${endpoint}/`;

const Steam = {
    getSteamID: identifier =>
        request({
            method: 'GET',
            url: API('getSteamID'),
            params: { identifier }
        })
    ,

    getFriends: steamid =>
        request({
            method: 'GET',
            url: API('getFriends'),
            params: { steamid }
        })
    ,

    getCommonApps: steamids =>
        request({
            method: 'GET',
            url: API('getCommonApps'),
            params: { steamids }
        })
    ,

    getAllSteamCategories: () =>
        request({
            method: 'GET',
            url: API('getAllSteamCategories')
        })
    ,
};

export default Steam;