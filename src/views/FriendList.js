import m from 'mithril';
import FriendTile from './FriendList/FriendTile';
import StagedFriend from './FriendList/StagedFriend';

const StyledListContainer =
    'div.flex.flex-wrap'
;

const FriendList = {
    view: ({ attrs: { state, actions } }) =>
        m('div',
            m('h2', 'selected friends'),
            m(StyledListContainer,
                Object.keys(state.stagedFriends).map(key =>
                    m(StagedFriend, {
                        avatar: state.stagedFriends[key].avatar,
                        personaname: state.stagedFriends[key].personaname,
                        profileurl: state.stagedFriends[key].profileurl,
                        steamid: state.stagedFriends[key].steamid,
                        unstageFriend: () => actions.unstageFriend(key)
                    })
                )
            ),

            m('h2', 'friends list'),
            m(StyledListContainer,
                state.friends.map(friend =>
                    m(FriendTile, {
                        avatar: friend.avatar,
                        personaname: friend.personaname,
                        showButton: !(friend.steamid in state.stagedFriends),
                        stageFriend: () => actions.stageFriend(friend)
                    })
                )
            )
        )
};

export default FriendList;