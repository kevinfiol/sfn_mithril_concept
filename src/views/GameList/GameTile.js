import m from 'mithril';

const StyledDiv =
    'div.p1.col.col-6.sm-col-3'
;

const StoreUrl = steam_appid => `https://store.steampowered.com/app/${ steam_appid }/`;

const GameTile = {
    view: ({ attrs: { header_image, name, steam_appid, platforms } }) =>
        m(StyledDiv,
            m('a', { href: StoreUrl(steam_appid) },
                m('img.rounded.fit', { src: header_image })
            ),

            m('div', name),

            // Platform Icons
            Object.keys(platforms).map(key =>
                platforms[key] &&
                    m('img', { src: `/img/${ key }.png` })
            )
        )
};

export default GameTile;