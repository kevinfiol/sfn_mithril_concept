import m from 'mithril';

import Btn from '../components/Btn';
import InputText from '../components/InputText';

const UserSubmission = ({ attrs: { actions } }) => {
    /**
     * Local State
     */
    let input = '';
    
    /**
     * Local Actions
     */
    const setInput = v => input = v;

    const onSearch = input => {
        actions.clearData();

        Promise.all([
            actions.getSteamID(input),
            actions.setUsername(input),
            actions.getFriends(input)
        ]).finally(() => {
            actions.setIsLoading(false);
            actions.setView('friends');
        });
    };

    const onCompare = (steamid, stagedFriends) => {
        const idArray = Object.keys(stagedFriends).map(key =>
            stagedFriends[key].steamid
        );

        const steamids = steamid + ',' + idArray.join(',');
        actions.getCommonApps(steamids)
            .finally(() => {
                actions.setView('games');
                actions.setIsLoading(false);
            })
        ;
    };

    return {
        view: ({ attrs: { state, actions } }) =>
            m('div',
                m(InputText, { value: input, oninput: setInput }),

                m('div.clearfix',
                    m(Btn, {
                        className: 'left',
                        disabled: state.isLoading,
                        onclick: () => onSearch(input)
                    }, 'Get Friends'),

                    Object.keys(state.stagedFriends).length > 0 &&
                        m(Btn, {
                            className: 'right',
                            disabled: state.view === 'games',
                            onclick: () => onCompare(state.steamid, state.stagedFriends)
                        }, 'Compare Games')
                    ,
                )
            )
    };
};

export default UserSubmission;