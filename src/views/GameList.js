import m from 'mithril';
import GameTile from './GameList/GameTile';

const StyledListContainer =
    'div.flex.flex-wrap'
;

const GameList = {
    view: ({ attrs: { state, actions } }) =>
        m('div',
            m(StyledListContainer,
                state.commonApps.map(a =>
                    m(GameTile, {
                        header_image: a.header_image,
                        name: a.name,
                        steam_appid: a.steam_appid,
                        platforms: a.platforms
                    })
                )
            )
        )
};

export default GameList;