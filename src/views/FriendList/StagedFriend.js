import m from 'mithril';
import Btn from '../../components/Btn';

const StyledDiv =
    'div.p1.col.col-12.sm-col-6'
;

const FriendTile = {
    view: ({ attrs: { avatar, personaname, profileurl, steamid, unstageFriend } }) =>
        m(StyledDiv,
            m('img.block', { src: avatar }),
            m('p',
                m('a', { href: profileurl }, personaname)
            ),

            m(Btn, { onclick: unstageFriend }, 'Remove')
        )
};

export default FriendTile;