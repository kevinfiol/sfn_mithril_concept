import m from 'mithril';
import Btn from '../../components/Btn';

const StyledDiv =
    'div.p1.col.col-12.sm-col-4.md-col-3'
;

const FriendTile = {
    view: ({ attrs: { avatar, personaname, showButton, stageFriend } }) =>
        m(StyledDiv,
            m('img.block', { src: avatar }),
            m('p', personaname),

            showButton &&
                m(Btn, { onclick: stageFriend }, 'Add')
        )
};

export default FriendTile;