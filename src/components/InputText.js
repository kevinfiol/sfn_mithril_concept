import m from 'mithril';

const StyledInput =
    'input.input.bg-black.white.p1'
;

const InputText = {
    view: ({ attrs: { oninput, value } }) =>
        m(StyledInput, {
            value,
            oninput: ({ target: { value } }) => {
                oninput(value);
            }
        })
};

export default InputText;